---
layout: default
---

[Gitlab](https://gitlab.com/PushFish/)<a class="item">Download (Coming soon)</a>

[Docs](https://gitlab.com/PushFish/PushFish-api/wikis/home)

# PushFish
----------

## Be in charge of your notifications.

[Get Started](https://gitlab.com/PushFish/)


### What is PushFish?

PushFish is a self-hostable, fully FOSS, third-party services free alternative to PushBullet using code from PushJet. The app will never use analytics or anything that sends back data to us.

### Easy-to-use API

Our API is built for developers, making it easy  
to integrate PushFish into your project.

### Open-Source

PushFish is community-developed under a BSD-License. Want to contribute? We are currently looking for Python and Android developers, join us on [Gitlab](https://gitlab.com/PushFish/) and [Slack](https://join.slack.com/t/pushjetfork/shared_invite/enQtNDMzNDk2MTYwOTQ5LWQ4ODZiMjQ3ZjQwNjQ4Y2NjMGMxNjVmYzNhNWZmNDgzMDg5NmY1YzA5NTIzMTk2NmRlMzI3YWE1OTE0ZDZkOTY)!

### Self-hosted

Want to be in control of your data? Host it yourself  
with our simple to follow tutorial.

### Encrypted

In future releases we also plan to support end-to-end encryption, because privacy matters!

### Cross-Platform

PushFish is in very early stages but we plan to support many platforms. Stay tuned!

[Gitlab](https://gitlab.com/PushFish/) [Docs](#) [Slack](https://join.slack.com/t/pushjetfork/shared_invite/enQtNDMzNDk2MTYwOTQ5LWQ4ODZiMjQ3ZjQwNjQ4Y2NjMGMxNjVmYzNhNWZmNDgzMDg5NmY1YzA5NTIzMTk2NmRlMzI3YWE1OTE0ZDZkOTY)




