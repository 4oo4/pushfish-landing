---
layout: page
permalink: /contributing/
title: Contributor's Guide
---

## Welcome to PushFish!

Interested in working on the PushFish server API, the PushFish Android app, or other PushFish infrastructure? Awesome! This guide will get you started.

This is a guide for contributing to the code bases maintained by the PushFish organization by [Coding](#coding), [Designing](#designing), [Writing](#writing), and [Testing](#testing).

Please note that by contributing to PushFish, that you acknowledge that you have read and agree to our [Contributor License Agreement](https://push.fish/cla). Questions? Contact us on [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI), or our [GitLab](https://gitlab.com/pushfish) and ask!

## Ecosystem Overview

Typical usage of PushFish does not involve a single piece of software, but several interacting components.

If you want to contribute to PushFish, there's definitely something for you! The first step is to figure out what project to work on.

### Core Protocol Components

| Component | Language (Toolset) | What Is It
--- | --- | ---
| [PushFish-api](https://gitlab.com/pushfish/pushfish-api) | Python | The Python code that powers the API itself, using [MQTT](https://mqtt.org).
| [PushFish-Py](https://gitlab.com/PushFish/pushrocket-py) | Python | A Python client API for PushFish. It sends Push notifications from Python scripts, and can be used to connect any command line application to PushFish!
| [PushFish-Connectors](https://gitlab.com/PushFish/pushrocket-Connectors) | Python | Allows users to listen for PushFish messages via TCP and websockets.


### Official Applications

| Application | Language (Toolset) | What Is It
--- | --- | ---
| [PushFish-Android](https://gitlab.com/PushFish/PushRocket-Android) | Java | The Android app that connects to the PushFish API server and MQTT.

### Websites

| Domain | Language (Toolset) | What Is It
--- | --- | ---
| [PushFish-Landing](https://gitlab.com/PushFish/pushfish-landing) | Javascript | You're here!

## Coding

A group of passionate, volunteer geeks are working to make PushFish awesome. Join us!

### How To Contribute Code

1. **Join the Community**. By joining our [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI), you can interact with other developers when you have questions, ideas, or problems.
1. **Identify the component you want to work on**. PushFish has code bases that deal with everything from cryptography and distributed systems to end-user graphic user-interfaces. We use Python, JavaScript, and Go. See [ecosystem overview](#ecosystem-overview) to find the right project.
1. **Get set up.** Each repo has a `README.md` file with clear instructions on how to get the repo up and running properly. Thanks, [Repository Standards](https://push.fish/repository-standards)!
1. **Find something to work on**. All actively developed repositories should have issues tagged "Good First Issues" specifically for new contributors. You are also welcome to work on something not currently filed if you have your own idea. Additionally, all repositories have contact information for the maintainer if you have trouble finding an issue to work on.
1. **Abide coding and commit standards**. Any specific information necessary to know in this regard should be in the project `README.md`.
1. **Commit early and ask questions**. Is an issue confusing? Please comment and say so! Not sure if you've got the right approach? Commit your code and we'll give feedback. Certain you're doing everything right? Commit it anyway. Once you commit, open a pull request. We encourage work-in-progress commits to let us know you're working on something and to facilitate feedback.
1. **Accept feedback and finish**. Most pull requests are reviewed within two business days. Once the repository maintainer has approved your contribution, it will get merged and you'll get a huge thank you!.

## Designing

Web and application designers are requested to be able to work directly on CSS in the project they'd be contributing to. Please follow the [Coding](#coding) instructions for any website or application you want to improve.

We're happy to provide assistance if you're used to working in HTML, CSS, and Javascript but are having trouble getting started. Check out our [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI).

## Writing

Most written content, and especially all technical writing, is checked into source control. To improve content we've written or add new content:

1. Identify which website or application it is in (see the [overview](#ecosystem-overview)).
1. Search for a quoted phrase of the content you want to change (or use the same technique to identify the folder to create a new document in).
1. Edit the content via the GitLab interface and submit it as a pull request.

## Testing

If you want to contribute without getting directly into the code, one of the best ways you can contribute is testing.

A number of our code bases ([PushFish-api](https://gitlab.com/pushfish/pushfish-api), [PushFish-Py](https://gitlab.com/PushFish/pushrocket-py), [PushFish-Android](https://gitlab.com/PushFish/PushRocket-Android)) go through regular release cycles where new versions are shipped every several weeks. Testing pre-release versions is a great way to help us identify issues and ship bug-free code.

### Ways to Test

- "Watch" the repo on GitLab. You will receive an email with release notes whenever a release candidate is out and you can [raise an issue](#raising-issues).
- Join the #early-testing channel in our [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI).
- For the hardcore, run master from source and/or the latest builds from [GitLab](https://gitlab.com/pushfish/pushfish-api).

Opening well-specified issues against release candidates or master builds is extremely useful in helping us create quality software.

**Note: Please do not use important data when testing. If using release candidates and especially master builds, back up your database and be cautious. While a substantial bug, like one that caused loss of data, you perform testing at your own risk.**

## Raising Issues

All [bug reports](#reporting-a-bug) and [feature requests](#feature-requests) are managed through GitLab.

If you're about to raise an issue because you've found a problem with PushFish, or you'd like to request a new feature, or for any other reason, please read this first.

### Reporting a Bug

A bug is a _demonstrable problem_ that is caused by the code in the repository. Good bug reports are extremely helpful - thank you!
Guidelines for bug reports:

1. **Identify the correct repo**. See [ecosystem overview](#ecosystem-overview). While it's okay if you get this wrong, it's a big help to us if you get it right.
2. **Check if the issue exists**. Please do a quick search to see if the issue has been reported (or fixed), including closed tickets.
3. **Follow the instructions** - When you open an issue inside of GitLab, each repo contains a template for how to create a good bug report. Santa _loves_ people who follow it.

Well-specified bug reports save developers lots of time and are [appreciated](#appreciation).

### Feature Requests

Feature requests are welcome. Before you submit one be sure to:

1. **Identify the correct repo**. See [ecosystem overview](#ecosystem-overview).
2. **Use the GitLab Issues search** and check the feature hasn't already been requested. Be sure to include closed tickets.
3. **Consider whether it's feasible** for us to tackle this feature in the next 6-12 months. The PushFish team is currently stretched thin just adding basic functionality. If this is a nice to have rather than a need, it is probably more clutter than helpful.
4. **Make a strong case** to convince the project's leaders of the merits of this feature. Please provide as much detail and context as possible. This means explaining the use case and why it is likely to be common.

### Discussion and Help

Sometimes, you may have a problem but an issue isn't appropriate (or you're not sure if it's a real issue).

Join our [Slack](https://join.slack.com/t/fosspush/shared_invite/enQtNDU3MzAzNTEwMTEzLWFhNzVjYWUxNTE5MjZkYzYzZWNjYzdmZDM4ZTc5ZGJjYjYyYTVmYmZkYzgwNTA4NmE4YzMwNjNlNjFjMzY0ZjI), or our [Gitlab](https://gitlab.com/pushfish) community and talk to others about it.

